package com.aierjun.webvedio.activity;

import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.aierjun.webvedio.R;
import com.aierjun.webvedio.utils.BackUtilsActivity;

public class MainActivity extends BackUtilsActivity {

    private EditText ed;
    private Button bt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findView();
        initView();
    }

    private void findView() {
        ed= (EditText) findViewById(R.id.ed_www);
        bt= (Button) findViewById(R.id.bt_www);
    }

    private void initView() {
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent();
                intent.putExtra("URI",ed.getText().toString());
                intent.setClass(MainActivity.this,TestPlayMediaInWebView.class);
                startActivity(intent);
            }
        });
    }
}
