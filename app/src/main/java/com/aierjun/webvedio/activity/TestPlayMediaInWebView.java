package com.aierjun.webvedio.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.aierjun.webvedio.R;
import com.aierjun.webvedio.utils.BackUtilsActivity;

/**
 * 添加权限<br/>
 * android.permission.INTERNET<br/>
 * 在manefest里面加上<br/>
 * android:hardwareAccelerated="true" <br/>
 * <br/>
 * 给webview添加两个<br/>
 * webView.setWebChromeClient(new WebChromeClient());<br/>
 * webView.setWebViewClient(new WebViewClient());<br/>
 * <br/>
 * 设置setting的属性 <br/>
 * 重点是setting.setJavaScriptEnabled(true);
 * 
 *
 * 
 * <br/>
 *         2014-3-26 下午9:30:26
 */
public class TestPlayMediaInWebView extends Activity {
	private WebView webView;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test_play_media_in_web_view);
		Intent intent=getIntent();
		String uri=intent.getStringExtra("URI");
		webView = (WebView) findViewById(R.id.webView);
		WebSettings setting = webView.getSettings();
		setSettings(setting);
		webView.setWebChromeClient(new WebChromeClient());
		webView.setWebViewClient(new WebViewClient());
		webView.loadUrl(uri);
	}

	@SuppressLint("NewApi")
	private void setSettings(WebSettings setting) {
		setting.setJavaScriptEnabled(true);
		setting.setBuiltInZoomControls(true);
		setting.setDisplayZoomControls(false);
		setting.setSupportZoom(true);

		setting.setDomStorageEnabled(true);
		setting.setDatabaseEnabled(true);
		// 全屏显示
		setting.setLoadWithOverviewMode(true);
		setting.setUseWideViewPort(true);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		webView.destroy();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
	}
}
